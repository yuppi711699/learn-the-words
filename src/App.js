import React from 'react';
// import logo from './logo.svg';
import './App.css';
import HeaderBlock from './components/HeaderBlock/HeaderBlock';
import ContentBlock from './components/contentBlock/ContentBlock';
import FooterBlock from './components/FooterBlock/FooterBlock';
import CardsBlock from './Card/index';
import Card from './Card/index'
import { wordsList } from './wordsList'


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <HeaderBlock />
        <div>
          {
            wordsList.map(({eng,rus}, index)=>(
              <Card key={index} eng={eng} rus={rus}/>)
            )}
        </div>
        <CardsBlock />
        <ContentBlock />
        <FooterBlock />
      </header>
    </div>
  );
}

export default App;
