import React from 'react';
// import reactDom from 'reactDom';
import Container from 'react-bootstrap/Container';
// import Navbar from 'react-bootstrap';

function  contentBlock (){
  const text = ['Заголовок 1', 'Заголовок 2' ,'Заголовок 3'];

  return (
    <Container>
      <h2>ContentBlock</h2>
      <ul>
        {text.map((item,index)=><li key={index}>{item}</li>)}
      </ul>
    </Container>
  )
}
export default contentBlock;